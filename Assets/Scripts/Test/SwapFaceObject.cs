using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System.Collections.Generic;
using System.Linq;

public class SwapFaceObject : MonoBehaviour {
    [SerializeField]
    ARFaceManager faceManager;
    [SerializeField]
    Material[] faceMaterials;
    public GameObject[] facePrefabs;
    int faceID = 0;
    int matID = 0;
    void Start()
    {

    }
     //Change with Model
    public void ChangeFaceModel(int id) {
        faceID = id;
        faceManager.facePrefab = facePrefabs[faceID];
    }

    public void ChangeFaceModel(string s) {
        s = s.ToLower();
        switch (s) {
            case "n":
                faceID++;
                if (faceID >= facePrefabs.Length) faceID = 0;
                break;
            case "p":
                faceID--;
                if (faceID < 0) faceID = facePrefabs.Length-1;
                break;
            default:
                Debug.Log("Case mismatch.");
                break;
        }
        faceManager.facePrefab = facePrefabs[faceID];
    }

    public void SwitchFaceMaterial(string s) {
        s = s.ToLower();
        switch (s) {
            case "n":
                matID++;
                if (matID >= faceMaterials.Length) matID = 0;
                break;
            case "p":
                matID--;
                if (matID < 0) matID = faceMaterials.Length - 1;
                break;
            default:
                Debug.Log("Case mismatch.");
                break;
        }
        foreach (ARFace face in faceManager.trackables) {
            face.GetComponent<Renderer>().material = faceMaterials[matID];
        }
    }
}
